package hamster.myalarm2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

//ПИШЕМ ПРИЕМНИК (BroadcastReceiver) ДЛЯ НАШЕГО ШИРОКОВЕЩАТЕЛЬНОГО СООБЩЕНИЯ, КОТОРОЕ МЫ ПОСЫЛАЕМ ИЗ AlarmFragment

public class AlarmBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        //1. получаем extra strings из intent (посланного из AlarmFragment-а)
        String alarmExtras = intent.getExtras().getString(RingtoneService.ALARM_AND_TIMER_EXTRAS_KEY);
        tryToStartAlarm(alarmExtras, context);

        //2. получаем extra strings из intent (посланного из TIMER)
        String getTimerExtraString = intent.getExtras().getString(RingtoneService.ALARM_AND_TIMER_EXTRAS_KEY);
        tryToStartTimer(getTimerExtraString, context);
    }

    //если получили данные из AlarmFragment-а, то пересылаем их в RingtoneService (pass the extra string from AlarmFragment to RingtoneService)
    private void tryToStartAlarm(String alarmExtras, Context context) {
        if (alarmExtras != null) {

            Intent serviceIntent = new Intent(context, RingtoneService.class);
            serviceIntent.putExtra(RingtoneService.ALARM_AND_TIMER_EXTRAS_KEY, alarmExtras);
            context.startService(serviceIntent);
        }
    }

    //// TODO: 20-Jul-18 Цей метод можна видалити, бо він дублює попередній.
    /// Але спочатку переконайся - заглянь в рінгтонСервіс, для чого точно використовуються ці екстра-дані.
    private void tryToStartTimer(String timerExtras, Context context) {
        if (timerExtras != null) {

            //pass the extra string from TimerActivity to RingtoneService
            Intent timerServiceIntent = new Intent(context, RingtoneService.class);
            timerServiceIntent.putExtra(RingtoneService.ALARM_AND_TIMER_EXTRAS_KEY, timerExtras);
            context.startService(timerServiceIntent);
        }

    }
}

