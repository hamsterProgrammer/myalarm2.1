package hamster.myalarm2;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.util.Log;

public class RingtoneService extends Service {

    public static final String ALARM_AND_TIMER_EXTRAS_KEY = "extras";

    MediaPlayer mediaPlayer;
    boolean isRunning;
    int startId;
    String stateOfBtn;

    NotificationManager notificationManager;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        //дістаємо еxtra дані з інтенту - вони приходять в наш Сервис з AlarmFragmentу (через посередництво AlarmBroadcastReceiverа)
        stateOfBtn = intent.getExtras().getString(ALARM_AND_TIMER_EXTRAS_KEY);

        // перевіряємо, яка кнопка натиснута - і взалежності від цього включаємо\виключаємо музику дзвінка
        ifElseStatements();

        return START_NOT_STICKY;
    }


    //в цьому м-ді перевіряємо, яка кнопка натиснута - і взалежності від того включаємо\виключаємо музику дзвінка
    private void ifElseStatements() {

        //this converts extra strings from the intent to startIds values (0 or 1)
        assert stateOfBtn != null;
        switch (stateOfBtn) {
            case "on":
                startId = 1;
                break;
            case "off":
                startId = 0;
                break;
            default:
                startId = 0;
                break;
        }

        //if music NO playing and user pressed "alarm on"
        //music should start playing
        if (!this.isRunning && startId == 1) {
            mediaPlayer = MediaPlayer.create(this, R.raw.alarm);
            mediaPlayer.start();

            sendNotif();

            this.isRunning = true;
            this.startId = 0;
        }

        //if music playing and the user pressed "alarm off"
        //music should stop playing
        else if (this.isRunning && startId == 0) {
            mediaPlayer.stop();
            mediaPlayer.reset();

            cancelNotification();

            this.isRunning = false;
            this.startId = 1;
        }

        //if there is no music playing and the user pressed "alarm off"
        //do nothing
        else if (!this.isRunning && startId == 0) {
            this.isRunning = false;
            this.startId = 0;
        }
        //if there is music playing and the user pressed "alarm on"
        //do nothing
        else if (this.isRunning && startId == 1) {
            Log.e("there is music, ", "and you want it on");

            this.isRunning = true;
            this.startId = 1;
        } else {
            Log.e("ELSE, ", "somehow you reached this)");
        }
    }

    //цей метод створює нотіфікейшн
    private void sendNotif() {
        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Intent intent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            Notification.Builder builder = new Notification.Builder(this);
            builder.setContentIntent(pendingIntent)
                    .setSmallIcon(R.drawable.clock)
                    .setWhen(System.currentTimeMillis())
                    .setAutoCancel(true)
                    .setContentTitle("New Notifications")
                    .setContentText("Stop your alarm if you waked up!");

            Notification notification = builder.getNotification();
            notificationManager.notify(1, notification);
        }
    }

    //цей метод видаляє нотіфікейшн
    public void cancelNotification() {
        notificationManager.cancelAll();
    }

    @Override
    public void onDestroy() {
        this.isRunning = false;
    }
}
