package hamster.myalarm2;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

public class MainActivity extends AppCompatActivity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager); //ViewPager - дозволяє організувати перегляд даних зі свайпанням сторінок вліво-вправо.
        setupViewPager(viewPager);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    //Додаємо в наш ViewPager AlarmFragment та TimerFragment
    private void setupViewPager(ViewPager viewPager) {
        MyViewPagerAdapter myViewPagerAdapter = new MyViewPagerAdapter(getSupportFragmentManager());
        myViewPagerAdapter.addFragment(new AlarmFragment(), "ALARM");
        myViewPagerAdapter.addFragment(new TimerFragment(), "TIMER");
        viewPager.setAdapter(myViewPagerAdapter);
    }
}
