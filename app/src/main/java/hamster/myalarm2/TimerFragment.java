package hamster.myalarm2;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import static android.app.PendingIntent.FLAG_NO_CREATE;
import static android.app.PendingIntent.FLAG_UPDATE_CURRENT;
import static android.content.Context.ALARM_SERVICE;

public class TimerFragment extends Fragment implements CompoundButton.OnCheckedChangeListener {

    TextView tvInfo;
    EditText etHour, etMinute, etSecond;
    ToggleButton toggleButton;

    BroadcastReceiver brReceiverForTimerCount;
    public final static String BROADCAST_ACTION = "hrymza_broadcast_action";

    AlarmManager alarmManager;
    Intent intentToAlarmReceiver;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_timer, null);

        toggleButton = (ToggleButton) view.findViewById(R.id.toggleButtonTimer);
        tvInfo = (TextView) view.findViewById(R.id.tvUpdate);
        etHour = (EditText) view.findViewById(R.id.etHour);
        etMinute = (EditText) view.findViewById(R.id.etMinute);
        etSecond = (EditText) view.findViewById(R.id.etSec);

        alarmManager = (AlarmManager) getActivity().getSystemService(ALARM_SERVICE);
        intentToAlarmReceiver = new Intent(getActivity(), AlarmBroadcastReceiver.class);

        //Створюємо BroadcastReceiver, який отримуватиме дані (inputTimeCounted) з MyTimerService и обновлятиме tvInfo
        brReceiverForTimerCount = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                int inputTimeCounted = intent.getIntExtra("inputTimeCounted", 0);

                //переводим отримані секунди в нормальний час (години, хвилини, секунди), щоб відобразити його в текстВю
                int hours = inputTimeCounted / 3600;
                int minutes = (inputTimeCounted % 3600) / 60;
                int seconds = inputTimeCounted % 60;
                String timeString = String.format("%02d:%02d:%02d", hours, minutes, seconds);
                tvInfo.setText(timeString);
            }
        };

        IntentFilter intentFilter = new IntentFilter(BROADCAST_ACTION); // створюємо intentFilter для нашого BroadcastReceiver
        getActivity().registerReceiver(brReceiverForTimerCount, intentFilter);//Реєструємо(включаемо) наш BroadcastReceiver. Тепер він отримуватиме Intentи, які відповідають умовам IntentFilter.

        //Перевіряємо, чи таймер вже виставлений. І, якщо так, то перевиставляємо йому раніше виставлений час
        checkIfTimerIsOn();

        toggleButton.setOnCheckedChangeListener(this);

        return view;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {

            //якщо editTexts пусті, то сервіс не стартуємо (return;) і кнопку не переключаємо
            if (etHour.getText().toString().equals("") && etMinute.getText().toString().equals("") && etSecond.getText().toString().equals("")) {
                toggleButton.setChecked(false);
                Toast.makeText(getActivity(), "Enter time!", Toast.LENGTH_LONG).show();
                return;
            }

            etHour.setVisibility(View.INVISIBLE);
            etMinute.setVisibility(View.INVISIBLE);
            etSecond.setVisibility(View.INVISIBLE);

            /**Передаємо в MyTimerService введений юзером час і стартуємо сервіс */
            Intent intent = new Intent(getActivity(), MyTimerService.class);
            intent.putExtra("inputTime", getTimeFromEt());
            getActivity().startService(intent);

            /**Створюємо PendingIntent (поміщаємо в нього intentToAlarmReceiver з extra "on" - по ньому наш RingtoneService знатиме, що юзер натиснув alarm_on).
             * Далі відправляємо наш pendingIntent через alarmManager м-дом set (на вхід йому іде: тип будильника, час спрацювання, наш pendingIntent)
             * АlarmManager спрацює у вказаний йому час і запустить наш pendingIntent. */
            intentToAlarmReceiver.putExtra(RingtoneService.ALARM_AND_TIMER_EXTRAS_KEY, "on");
            PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), 1, intentToAlarmReceiver, FLAG_UPDATE_CURRENT);
            alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + getTimeFromEt() * 1000, pendingIntent);

        } else {
            Intent intent2 = new Intent(getActivity(), MyTimerService.class);
            getActivity().stopService(intent2);

            PendingIntent existingPendingIntent = PendingIntent.getBroadcast(getActivity(), 1, new Intent(getActivity(), AlarmBroadcastReceiver.class), FLAG_NO_CREATE);
            if (existingPendingIntent != null) {
                alarmManager.cancel(existingPendingIntent);
                existingPendingIntent.cancel();
            }

            /**відправляємо наш intentToAlarmBrReceiver (через sendBroadcast) з extra "on" - по ньому наш RingtoneService знатиме, що юзер натиснув alarm_off  і зупинить рінгтон*/
            intentToAlarmReceiver.putExtra(RingtoneService.ALARM_AND_TIMER_EXTRAS_KEY, "off");
            getActivity().sendBroadcast(intentToAlarmReceiver);

            //обнуляємо TextViews та EditTexts; а також робиом невидимими EditTexts
            tvInfo.setText("00:00:00");
            etHour.setText("");
            etMinute.setText("");
            etSecond.setText("");
            etHour.setVisibility(View.VISIBLE);
            etMinute.setVisibility(View.VISIBLE);
            etSecond.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // дерегистрируем (выключаем) BroadcastReceiver
        getActivity().unregisterReceiver(brReceiverForTimerCount);
    }

    //переводимо введені в едитТексти числа в нормальний час
    private int getTimeFromEt() {
        String strHour = etHour.getText().toString();
        if (strHour.isEmpty()) {
            strHour = "0";
        }
        int intHour = Integer.valueOf(strHour);

        String strMinute = etMinute.getText().toString();
        if (strMinute.isEmpty()) {
            strMinute = "0";
        }
        int intMinute = Integer.valueOf(strMinute);

        String strSec = etSecond.getText().toString();
        if (strSec.isEmpty()) {
            strSec = "0";
        }
        int intSec = Integer.valueOf(strSec);

        int hourToSeconds = intHour * 60 * 60;
        int minuteToSeconds = intMinute * 60;

        int secondsForTimer = hourToSeconds + minuteToSeconds + intSec;

        return secondsForTimer;
    }

    /**
     * Цей метод перевіряє, чи таймер вже виставлений (шляхом перевірки чи даний пендінгІнтент вже існує в системі).
     * І, якщо таймер вже виставлений, то робимо кнопку включеною і видаляємо з екрану едітТексти
     */
    private void checkIfTimerIsOn() {
        PendingIntent existingPendingIntent = PendingIntent.getBroadcast(getActivity(), 1, new Intent(getActivity(), AlarmBroadcastReceiver.class), FLAG_NO_CREATE);
        boolean isTimerOn = existingPendingIntent != null;

        if (isTimerOn) {
            toggleButton.setChecked(true);
            etHour.setVisibility(View.INVISIBLE);
            etMinute.setVisibility(View.INVISIBLE);
            etSecond.setVisibility(View.INVISIBLE);
        }
    }


}

