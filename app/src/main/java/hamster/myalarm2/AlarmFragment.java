package hamster.myalarm2;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.ToggleButton;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import static android.app.PendingIntent.FLAG_NO_CREATE;
import static android.app.PendingIntent.FLAG_UPDATE_CURRENT;
import static android.content.Context.ALARM_SERVICE;
import static android.content.Context.MODE_PRIVATE;

/**
 * PendingIntent - дозволяє сторонньому додатку (в котрий його передали) запустити Intent, що зберігається в цьому PendingIntent.
 * Створюється PendingIntent так:
 * 1) Створюємо Intent з даними. 2) Створюємо PendingIntent (одним з м-дів: getActivity, getBroadcast, getService).
 * <p>
 * AlarmManager - спрацьовує (тобто, використовує PendingIntent, який йому відправите)по розкладу.
 * PendingIntent відправимо м-дом set. На вхід AlarmManager потребує: тип будильника, час спрацювання, PendingIntent.
 */

public class AlarmFragment extends Fragment implements CompoundButton.OnCheckedChangeListener {

    TimePicker timePicker;
    TextView updateText;
    ToggleButton toggleButton;
    AlarmManager alarmManager;
    Calendar calendar;
    Intent intentToAlarmBrReceiver;
    SharedPreferences sharedPreferences;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_alarm, null);
        timePicker = (TimePicker) view.findViewById(R.id.timePicker);
        timePicker.setIs24HourView(true);
        updateText = (TextView) view.findViewById(R.id.tvUpdate);
        toggleButton = (ToggleButton) view.findViewById(R.id.toggleButton);

        alarmManager = (AlarmManager) getActivity().getSystemService(ALARM_SERVICE);
        calendar = Calendar.getInstance(); //м. getInstance() - повертає об. кл. Calendar, ініційований поточною датою і часом.
        intentToAlarmBrReceiver = new Intent(getActivity(), AlarmBroadcastReceiver.class);

        checkIfAlarmIsOn(); //Перевіряємо, чи будильник вже виставлений. І якщо так - перевиставляємо йому раніше виставлений час.

        toggleButton.setOnCheckedChangeListener(this);

        return view;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            int timePickerHour = timePicker.getCurrentHour();
            int timePickerMinute = timePicker.getCurrentMinute();
            int seconds = 0;

            //встановлюємо час з таймПекера в наш обєкт Calendar
            calendar.set(Calendar.HOUR_OF_DAY, timePickerHour);
            calendar.set(Calendar.MINUTE, timePickerMinute);
            calendar.set(Calendar.SECOND, seconds);

            //показуємо в TextView засечений в таймПікері час
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm"); //щоб вивести час в текстВю в правильному форматі
            String timeToShow = sdf.format(calendar.getTime());
            updateText.setText("Alarm set to " + timeToShow);

            /**Створюємо PendingIntent (поміщаємо в нього intentToAlarmBrReceiver з extra "on" - по ньому наш RingtoneService знатиме, що юзер натиснув alarm_on)*/
            intentToAlarmBrReceiver.putExtra(RingtoneService.ALARM_AND_TIMER_EXTRAS_KEY, "on");
            PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), 0, intentToAlarmBrReceiver, FLAG_UPDATE_CURRENT);

            /**відправляємо наш pendingIntent через alarmManager м-дом set (на вхід йому іде: тип будильника, час спрацювання, наш pendingIntent)
             * АlarmManager спрацює у вказаний йому час і запустить наш pendingIntent. */
            long currentTime = Calendar.getInstance().getTimeInMillis(); //отримуємо теперішній час (в мілісекундах)
            long calendarMillis = calendar.getTimeInMillis(); //отримуємо виставлений юзером час (в мілісекундах)
            if (currentTime > calendarMillis) { //якщо виставлений час менший за теперішній, то до виставленого часу додаємо 24 год. (в мілісек.)
                calendarMillis = calendarMillis + 86400000;
            }

            saveTimeFromTimePicker(calendarMillis); //зберігаємо виставлений в таймПікері час (*щоб при відкритті activity після її смерті, будильник вже був виставлений)

            alarmManager.set(AlarmManager.RTC_WAKEUP, calendarMillis, pendingIntent);
        } else {

            PendingIntent existingPendingIntent = PendingIntent.getBroadcast(getActivity(), 0, new Intent(getActivity(), AlarmBroadcastReceiver.class), FLAG_NO_CREATE);
            if (existingPendingIntent != null) {
                alarmManager.cancel(existingPendingIntent);
                existingPendingIntent.cancel();
            }

            /**відправляємо наш intentToAlarmBrReceiver (через sendBroadcast) з extra "on" - по ньому наш RingtoneService знатиме, що юзер натиснув alarm_off  і зупинить рінгтон*/
            intentToAlarmBrReceiver.putExtra(RingtoneService.ALARM_AND_TIMER_EXTRAS_KEY, "off");
            getActivity().sendBroadcast(intentToAlarmBrReceiver);
            updateText.setText("Alarm is OFF!");
        }
    }

    /**
     * Цей метод перевіряє, чи вже виставлений будильник (шляхом перевірки чи даний пендінгІнтент вже існує в системі).
     * І, якщо буд-к вже виставлений, то перевиставляємо йому раніше засечений час.
     * (щоб при перестворенні activity після її вбивства, виставлений час не збивався)
     */
    private void checkIfAlarmIsOn() {
        PendingIntent existingPendingIntent = PendingIntent.getBroadcast(getActivity(), 0, new Intent(getActivity(), AlarmBroadcastReceiver.class), FLAG_NO_CREATE);
        boolean isAlarmOn = existingPendingIntent != null;

        if (isAlarmOn) {

            toggleButton.setChecked(true);

            //встановлюємо попередньо засечений час з таймПікера в наш обєкт Calendar
            long settedTimeFromTimePicker = getSavedTimeFromTimePicker();
            calendar.setTimeInMillis(settedTimeFromTimePicker);

            //показуэмо обраний в таймПікері час в едітТексті
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm"); //щоб вивести час в текстВю в правильному форматі
            String timeToShow = sdf.format(calendar.getTime());
            updateText.setText("Alarm set to " + timeToShow);

            //оновляємо таймПікер попередньо встановленим часом
            int calendarHour = calendar.get(Calendar.HOUR_OF_DAY);
            int calendarMinute = calendar.get(Calendar.MINUTE);
            timePicker.setCurrentHour(calendarHour);
            timePicker.setCurrentMinute(calendarMinute);
        }
    }

    // Цей метод зберігає обраний в таймПікері час
    public void saveTimeFromTimePicker(long timeFromTimePicker) {
        sharedPreferences = getActivity().getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong("timePickerTime", timeFromTimePicker);
        editor.commit();
    }

    // Цей метод вертає збережений час, який був обраний в таймПікері
    public long getSavedTimeFromTimePicker() {
        sharedPreferences = getActivity().getPreferences(MODE_PRIVATE);
        long timeFromTimePicker = sharedPreferences.getLong("timePickerTime", 0);
        return timeFromTimePicker;
    }

}
