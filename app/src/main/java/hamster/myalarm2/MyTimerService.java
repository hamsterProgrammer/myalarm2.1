package hamster.myalarm2;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;

import hamster.myalarm2.R;

import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

import java.util.Timer;
import java.util.TimerTask;

/**
 * З допомогою класів Timer і TimerTask можемо планувати запуск задачі у визначений час.
 * З доп. Timer плануємо час виконання задачі, яка має бути екзампляром класу TimerTask.
 */

public class MyTimerService extends Service {

    int inputTime;
    String inputTimeString;
    private Timer mTimer;
    private MyTimerTask mMyTimerTask;
    NotificationCompat.Builder newBuilder;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        inputTime = intent.getIntExtra("inputTime", 60); //отримуємо час, введений юзером (з TimerFragment)

        cancelExistingTask(); //відміняємо існуючу задачу і заново плунуємо її виконання
        startNewTask();

        notifForeground();//notification Foreground (не вмирає при нестачі памяті)

        return super.onStartCommand(intent, flags, startId);
    }

    //планує виконання задачі через певний час (з періодичном повтором виконання)
    private void startNewTask() {
        mTimer = new Timer();
        mMyTimerTask = new MyTimerTask();
        mTimer.schedule(mMyTimerTask, 0, 1000); //м. schedule (TimerTask task, long delay, long period) - планує виконання задачі (task) через період (delay) в мс. З повтором задачі - кожні period мсекунд.
    }

    //відміняє існуючу задачу (якщо вона існує)
    private void cancelExistingTask() {
        if (mMyTimerTask != null)
            mMyTimerTask.cancel();
        if (mTimer != null)
            mTimer.cancel();
    }

    //В цьому м-ді робимо notification Foreground (не вмирає при нестачі памяті)
    public void notifForeground() {
        //при натисненні на notification, відкриється МеінАктівіті
        Intent notifIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notifIntent, 0);

        //білдимо notification
        newBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.arrow_icon)
                .setContentTitle("Timer")
                .setContentText(inputTimeString)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent);
        startForeground(1, newBuilder.build());
    }

    /**
     * Кл. TimerTask - реалізує інтерф. Runnable і може бути використаний для створення потоку виконання.
     * Перевизначаємо м. run()– він повинен містити код, який буде виконувати наша задача.
     */
    private class MyTimerTask extends TimerTask {
        @Override
        public void run() {
            int inputTimeCounted = timerCount(); //этот м. отнимает от inputTime единицу и возвращает получившееся значение
            inputTimeCounted++; //щоб рахувало час правильно (від початково введеної цифри, а не від вже зменшеної на одиницю)

            //надсилаємо вродкас з часом
            Intent intent = new Intent(TimerFragment.BROADCAST_ACTION);
            intent.putExtra("inputTimeCounted", inputTimeCounted);
            sendBroadcast(intent);

            //оновлюємо notification Foreground (щоб воно відображало зворотній відлік таймеру)
            newBuilder.setContentText(String.valueOf(inputTimeString));
            startForeground(1, newBuilder.build());
        }

        //м. timerCount() - віднімає одиницю від введеного юзером часу і повертає отримане значення
        private int timerCount() {
            if (inputTime == 0) mMyTimerTask.cancel();

            //конвертуємо inputTime (час, введений юзер) в нормальний формат часу
            int hours = inputTime / 3600;
            int minutes = (inputTime % 3600) / 60;
            int seconds = inputTime % 60;
            inputTimeString = String.format("%02d:%02d:%02d", hours, minutes, seconds);

            inputTime--;

            return inputTime;
        }
    }

    @Override
    public void onDestroy() {
        //при отменении (выключении) сервиса наша задача (MyTimerTask) будет останавливаться
        cancelExistingTask();
        super.onDestroy();
    }
}
