package hamster.myalarm2;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.ListFragment;

import java.util.ArrayList;
import java.util.List;

//ViewPager - позволяет организовать просмотр данных с возможностью перелистывания страниц влево-вправо.
//Для предоставления данных ViewPagerу нужен еще PagerAdapter – это базовый абстр. класс, для которого мы дописываем свою реализацию.
//Стандартная реализация PagerAdapter - FragmentPagerAdapter (работает с фрагментами). Нам надо лишь создать фрагмент и определить количество страниц.


public class MyViewPagerAdapter extends FragmentPagerAdapter {

    private final List<Fragment> fragmentList = new ArrayList<>();
    private final List <String> fragmentsTitleList = new ArrayList<>();

    public MyViewPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override //возвращает елемент нашего списка по позиции
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override //возвращает количество элементов в нашем списке
    public int getCount() {
        return fragmentList.size();
    }

    @Override //возвращает заголовок для конкретной страницы
    public CharSequence getPageTitle(int position) {
        return fragmentsTitleList.get(position);
    }

    //добавляет фрагмент и его название (идут на вход м-да) в наш список
    public void addFragment (Fragment fragment, String title) {
        fragmentList.add(fragment);
        fragmentsTitleList.add(title);
    }
}
